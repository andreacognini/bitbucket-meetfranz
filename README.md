# Installation

1. To install a new integration, download the plugin
2. Open the Franz Plugins folder on your machine:
	* Mac: `~/Library/Application\ Support/Franz/Plugins/`
	* Windows: `%appdata%/Franz/Plugins`
	* Linux: `~/.config/Franz/Plugins`
	* Alternatively: Go to your Franz settings page, scroll down to the bottom and you will see an option to "*Open the Franz plugin directory*"
3. Copy the plugin folder into the plugins directory
4. Restart Franz
